import main


def check_value():
    v1 = 1
    v2 = main.check_value(v1)
    assert v2 == 1


def min_value():
    arr = [1, 2, 3, 4, 5]
    min_index = main.min_value(arr)
    assert min_index == 0


def find_index_value():
    arr = [1, 2, 3]
    value = 1
    i = main.find_index_value(arr, value)
    assert i == 0


def quickSort():
    arr = [3, 4, 1]
    arr1 = main.quickSort(arr)
    assert arr1 == [1, 3, 4]


def highest_sequence():
    arr = [1, 1, 2, 5]
    ml = main.highest_sequence(arr)
    ms = main.highest_sequence(arr)
    assert ml == 2, ms == 0


def three_highest_value():
    arr = [2, 3, 1, 5, 3, 4, 6]
    t1 = main.three_highest_value(arr)
    t2 = main.three_highest_value(arr)
    t3 = main.three_highest_value(arr)
    assert t1 == 6 & t2 == 5 & t3 == 4


def lists_equality():
    arr = [1, 2, 3]
    arr1 = [1, 2, 3]
    v = main.lists_equality(arr, arr1)
    assert v == True