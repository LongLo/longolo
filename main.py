import copy
import random


def check_value(value):
    while True:
        if value.isdigit():
            return int(value)
        else:
            try:
                float(value)
                return float(value)
            except ValueError:
                pass
        value = input("Введи число:")


def min_value(arr):
    min_index = 0
    for i in range(len(arr)):
        if arr[min_index] > arr[i]:
            min_index = i
    return min_index


def find_index_value(arr, value):
    for i in range(len(arr)):
        if value == arr[i]:
            return i
    return "Не найдено"


def quickSort(array):
    if len(array) <= 1:
        return array

    lower_array = []
    upper_array = []
    equal_pivot = []
    pivot = array[(len(array) - 1) // 2]
    for digit in array:
        if digit < pivot:
            lower_array.append(digit)
        elif digit > pivot:
            upper_array.append(digit)
        else:
            equal_pivot.append(digit)

    return quickSort(lower_array) + equal_pivot + quickSort(upper_array)


def highest_sequence(arr):
    if len(arr) == 1:
        return 1, 0
    maxlen, maxstart = 1, 0
    temp_len, temp_start = 1, 0
    for i in range(1, len(arr)):
        if arr[i] != arr[i - 1]:
            temp_len = 1
            temp_start = i
        elif arr[i] == arr[i - 1]:
            temp_len += 1
        if temp_len > maxlen:
            maxlen, maxstart = temp_len, temp_start
    return maxlen, maxstart


def three_highest_value(arr):
    temp = copy.deepcopy(arr)
    temp = quickSort(temp)
    return temp[-1], temp[-2], temp[-3]


def lists_equality(list_a, list_b):
    if list_a == list_b:
        return True
    return False


def add_list():
    new_list = []
    num = input("Введите кол-во элементов")
    while True:
        if num.isdigit():
            num = int(num)
            print("1 - Случайными числами\n"
                  "2 - Ручками\n")
            choice = input()
            if choice == "1":
                for i in range(num):
                    new_list.append(random.randint(-num, num))
            if choice == "2":
                for i in range(num):
                    value = input("Введи число")
                    value = check_value(value)
                    new_list.append(value)
            return new_list
        num = input("Введите кол-во элементов")


def list_choice(dict_of_list):
    for i in range(len(dict_of_list)):
        print(f'{i} - {dict_of_list[i]}')
    while True:
        value = input("Выбери массив:")
        if value.isdigit():
            value = int(value)
            if value < len(dict_of_list):
                return value


def menu_choice():
    print("1 - Ввод массивов\n"
          "2 - Сортировка массива\n"
          "3 - Нахождение индекса элемента\n"
          "4 - Нахождение мин. элемента\n"
          "5 - Нахождение самой длинной последовательности\n"
          "6 - Нахождение трех самых больших элементов\n"
          "7 - Сравнение массивов\n"
          "8 - Вывести массивы")


def main():
    dict_of_lists = []
    actions = {2: lambda a: quickSort(a),
               3: lambda a, b: print(find_index_value(a, b)),
               4: lambda a: print(min_value(a)),
               5: lambda a: print(highest_sequence(a)),
               6: lambda a: print(three_highest_value(a)),
               7: lambda a, b: print(lists_equality(a, b)),
               }

    while True:
        menu_choice()
        x = input()
        if x.isdigit():
            x = int(x)

            if x == 1:
                new_list = add_list()
                if len(new_list) > 0:
                    dict_of_lists.append(new_list)
            if x == 2:
                index = list_choice(dict_of_lists)
                dict_of_lists[index] = actions[x](dict_of_lists[index])
            if x == 3:
                index = list_choice(dict_of_lists)
                value = input("Введи число:")
                value = check_value(value)
                actions[x](dict_of_lists[index], value)
            if 3 < x < 7:
                if len(dict_of_lists) > 0:
                    index = list_choice(dict_of_lists)
                    actions[x](dict_of_lists[index])
                else:
                    print("Нет массивов")
            if x == 7:
                if len(dict_of_lists) > 1:
                    index_a = list_choice(dict_of_lists)
                    index_b = list_choice(dict_of_lists)
                    actions[7](dict_of_lists[index_a], dict_of_lists[index_b])
                else:
                    print("Недостаточно массивов")
            if x == 8:
                for item in dict_of_lists:
                    print(item)


if __name__ == '__main__':
    main()
